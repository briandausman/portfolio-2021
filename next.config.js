// uncomment to use mdx
// const withMDX = require('@next/mdx')({
//   extension: /\.mdx?$/,
// });

// module.exports = withMDX({
//   pageExtensions: ['mdx'],
// });

// generate sitemap
module.exports = {
  webpack: (config, { isServer }) => {
    if (isServer) require('./components/lib/generate-sitemap.js');
    return config;
  },
};
