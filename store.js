import { useMemo } from 'react';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';

let store;

const initialState = {
  navOpen: false,
  contactOpen: false,
  test: 'Redux working' /* Delete ME */,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'TOGGLE_CONTACT':
      return {
        ...state,
        contactOpen: action.payload,
        navOpen: false,
      };
    case 'INITIAL_DATA':
      return {
        ...state,
        ...action.payload,
      };
    case 'MQ':
      return {
        ...state,
        mq: action.payload,
      };
    case 'TOGGLE_NAV':
      return {
        ...state,
        navOpen: action.payload,
      };
    /* Delete ME */
    case 'UPDATE_TEST':
      return {
        ...state,
        test: action.payload,
      };
    default:
      return state;
  }
};

function initStore(preloadedState = initialState) {
  return createStore(
    reducer,
    preloadedState,
    composeWithDevTools(applyMiddleware()),
  );
}

export const initializeStore = (preloadedState) => {
  let _store = store ?? initStore(preloadedState);

  // After navigating to a page with an initial Redux state, merge that state
  // with the current state in the store, and create a new store
  if (preloadedState && store) {
    _store = initStore({
      ...store.getState(),
      ...preloadedState,
    });
    // Reset the current store
    store = undefined;
  }

  // For SSG and SSR always create a new store
  if (typeof window === 'undefined') return _store;
  // Create the store once in the client
  if (!store) store = _store;

  return _store;
};

export function useStore(initialState) {
  const store = useMemo(() => initializeStore(initialState), [initialState]);
  return store;
}
