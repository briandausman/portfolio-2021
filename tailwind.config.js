module.exports = {
  purge: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
    './layout/**/*.{js,ts,jsx,tsx}',
    './helpers/**/*.{js,ts,jsx,tsx}',
  ],
  darkMode: 'media', // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        nwcPurple: '#9300FF',
        nwcGreen: '#03E7FF',
        nwcBlue: '#00EBFF',
        nwcYellow: '#F3FE69',
        nwcOrange: '#FFAD3C',
        nwcPink: '#FF38DA',
      },
      fontSize: {
        big: '225px',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
