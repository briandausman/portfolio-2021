const useIsInView = (id: string): boolean => {
  const el = document.getElementById(id);
  const isVisible = isInViewport(el) ? true : false;
  return isVisible;
};

const isInViewport = (el) => {
  if (!el) return false;

  const rect = el.getBoundingClientRect();

  return (
    rect.top >= 0 &&
    rect.left >= 0 &&
    rect.bottom <=
      (window.innerHeight || document.documentElement.clientHeight) &&
    rect.right <= (window.innerWidth || document.documentElement.clientWidth)
  );
};

export default useIsInView;
