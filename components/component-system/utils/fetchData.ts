/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
const baseUrl = process.env.NEXT_PUBLIC_STRAPI_URL;

const fetchData = async (path, params = null) => {
  let url;

  if (params !== null) {
    url = `${baseUrl}/${path}/${params}`;
  } else {
    url = `${baseUrl}/${path}`;
  }

  const response = await fetch(`${url}`);
  const data = await response.json();
  return data;
};

export default fetchData;
