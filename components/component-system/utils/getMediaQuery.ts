/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
const getMediaQuery = (headers) => {
  const UA = headers['user-agent'];
  const isMobile = Boolean(
    UA.match(
      /Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile|WPDesktop/i,
    ),
  );

  return isMobile;
};

export default getMediaQuery;
