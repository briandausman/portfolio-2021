import Head from 'next/head';
import { ReactChild, useEffect } from 'react';

interface Props {
  children?: ReactChild;
  domain: string;
  meta?: Meta[];
  og?: any;
  gaId?: string;
  googleMapsApiKey?: string;
  googleTagManagerId?: string;
  heading?: string;
  protocol?: string;
  title: string;
}

interface Meta {
  name: string;
  content: string;
}

const Seo: any = ({
  children,
  domain = null,
  gaId = '',
  googleMapsApiKey = '',
  googleTagManagerId = '',
  // heading = '',
  meta = [],
  og = {},
  protocol = 'https:',
  title = 'Hello World!',
}: Props) => {
  useEffect(() => {
    if (googleMapsApiKey !== '') {
      // const head = document.getElementsByTagName('head')[0];
      // const script = document.createElement('script');
      // script.type = 'text/javascript';
      // script.src = `https://maps.googleapis.com/maps/api/js?key=${googleMapsApiKey}`;
      // head.appendChild(script);
    }
  }, []);

  return (
    <Head>
      <title>{title}</title>
      <meta httpEquiv="x-ua-compatible" content="ie=edge" />
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1, shrink-to-fit=no"
      />
      <link rel="sitemap" type="application/xml" href="/sitemap.xml" />

      {domain !== null && (
        <link
          rel="canonical"
          href={`${protocol}//${domain}/`}
          data-baseprotocol={protocol}
          data-basehost={domain}
        />
      )}

      {meta.map((item) => (
        <meta key={item.name} name={item.name} content={item.content} />
      ))}

      {Object.keys(og).map((key) => (
        <meta property={`og:${key}`} content={og[key]} key={og[key]} />
      ))}

      {googleTagManagerId !== '' && (
        <script
          dangerouslySetInnerHTML={{
            __html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','${googleTagManagerId}');`,
          }}
        />
      )}

      {/* Google Analytics */}
      {gaId !== '' && (
        <script
          async
          src={`https://www.googletagmanager.com/gtag/js?id=${gaId}`}
        />
      )}

      {gaId !== '' && (
        <script
          dangerouslySetInnerHTML={{
            __html: `
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());
              gtag('config', '${gaId}', {
                page_path: window.location.pathname,
              });`,
          }}
        />
      )}
      {children}
    </Head>
  );
};

export default Seo;
