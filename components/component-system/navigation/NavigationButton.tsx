interface Props {
  /**
   * Function which will return when the button is clicked
   */
  callback: () => void;
  /**
   * Color of  the icons
   */
  color?: string;
  /**
   * What will show as the button's icon, use state to switch to close if needed
   */
  icon?: IconEnum;
  /**
   * Size, in REMs of the button, 12 is about 48px
   */
  size?: number;
  /**
   * Custom classes applied to the button
   */
  theme?: string;
  /**
   * Screen reader aria-label title
   */
  title?: string;
}

type IconEnum = 'bars' | 'dots' | 'close' | 'desc' | 'asc';

const NavigationButton = ({
  callback,
  color = '#2c2c2c',
  icon = 'bars',
  size = 12,
  theme = '',
  title = 'open navigation',
}: Props): JSX.Element => {
  const iconStyles = {
    margin: 'auto',
    align: 'center',
    fill: color,
  };

  return (
    <div
      id="NavigationButton"
      className="nwc--navigation-button"
      data-test="nwc--navigation-button"
    >
      <button
        aria-label={title}
        onClick={() => callback()}
        className={`inline-block align-middle w-${size} h-${size} ${theme}`}
      >
        {icon === 'bars' && (
          <svg style={iconStyles} viewBox="0 0 100 60" width="40" height="40">
            <rect width="100" height="6"></rect>
            <rect y="25" width="100" height="6"></rect>
            <rect y="50" width="100" height="6"></rect>
          </svg>
        )}

        {icon === 'asc' && (
          <svg style={iconStyles} viewBox="0 0 100 60" width="40" height="40">
            <rect width="100" height="6"></rect>
            <rect y="25" width="75" height="6"></rect>
            <rect y="50" width="50" height="6"></rect>
          </svg>
        )}

        {icon === 'desc' && (
          <svg style={iconStyles} viewBox="0 0 100 60" width="40" height="40">
            <rect width="50" height="6"></rect>
            <rect y="25" width="75" height="6"></rect>
            <rect width="100" y="50" height="6"></rect>
          </svg>
        )}

        {icon === 'dots' && (
          <svg style={iconStyles} viewBox="0 0 100 60" width="40" height="40">
            <circle cx="50%" cy="0" r="6" />
            <circle cx="50%" cy="30" r="6" />
            <circle cx="50%" cy="60" r="6" />
          </svg>
        )}

        {icon === 'close' && (
          <svg
            style={{ ...iconStyles, paddingLeft: 10 }}
            viewBox="0 0 100 50"
            stroke={color}
            strokeWidth="6"
            width="40"
            height="40"
          >
            <line x2="50%" y2="50" />
            <line x1="50%" y2="50" />
          </svg>
        )}
      </button>
    </div>
  );
};

export default NavigationButton;
