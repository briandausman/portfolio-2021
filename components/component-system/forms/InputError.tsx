import React from 'react';

interface Props {
  error?: any;
}
const InputError: any = ({ error }: Props) => {
  if (!error || error === '') return <></>;

  return (
    <span className="bg-red-600 text-white text-xs p-1 absolute -bottom-3 right-0.5">
      {error}
    </span>
  );
};

export default InputError;
