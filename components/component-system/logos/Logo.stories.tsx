import React from 'react';
import Logo from './Logo';

export default {
  title: 'Logos/NWC Logo',
  component: Logo,
};

const Template = (args) => <Logo {...args} />;

const defaultProps = {
  alt: 'Logo - Click to go home',
  src:
    'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjI3OS4yNjQiIHZpZXdCb3g9IjAgMCA1MTIgMjc5LjI2NCI+CiAgPGRlZnM+CiAgICA8bGluZWFyR3JhZGllbnQgaWQ9ImxpbmVhci1ncmFkaWVudCIgeDI9IjEiIHkyPSIxIiBncmFkaWVudFVuaXRzPSJvYmplY3RCb3VuZGluZ0JveCI+CiAgICAgIDxzdG9wIG9mZnNldD0iMCIgc3RvcC1jb2xvcj0iI2YyMzNiOSIvPgogICAgICA8c3RvcCBvZmZzZXQ9IjEiIHN0b3AtY29sb3I9IiM5MDEyZmUiLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgPC9kZWZzPgogIDxwYXRoIGlkPSJQYXRoXzEiIGRhdGEtbmFtZT0iUGF0aCAxIiBkPSJNMTYuOTQzLDYxMy42MzJWNTY3LjA4NUgtMjkuNlY1MjAuNTM3SC03Ni4xNTFWNDc0SC0xMjIuN3Y0Ni41NDdILTI2Mi4zM1Y0NzRoLTQ2LjUzN3Y0Ni41NDdoLTQ2LjU0N3Y0Ni41NDdoLTQ2LjU0N3Y0Ni41MzdILTQ0OC41Vjc1My4yNjRoNDYuNTQ3VjY2MC4xNzloNDYuNTQ3djkzLjA4NWg0Ni41NDdWNzA2LjcxNkgtNzYuMTQxdjQ2LjU0OGg0Ni41NDdWNjYwLjE3OUgxNi45NTN2OTMuMDg1SDYzLjVWNjEzLjYzMlptLTkzLjA4NS00Ni41NDd2NDYuNTQ3aC00Ni41NDdWNTY3LjA4NVptLTIzMi43MTcsMGg0Ni41NDd2NDYuNTQ3aC00Ni41NTdaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSg0NDguNSAtNDc0KSIgZmlsbD0idXJsKCNsaW5lYXItZ3JhZGllbnQpIi8+Cjwvc3ZnPgo=',
};

export const Default = Template.bind({});
Default.args = {
  ...defaultProps,
  size: null,
};

export const Width = Template.bind({});
Width.args = {
  ...defaultProps,
  width: 25,
};
