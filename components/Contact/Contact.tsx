import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Input, Mask, useEmailValidator } from '../component-system';

const Contact: React.FC = () => {
  const dispatch = useDispatch();
  const contactOpen = useSelector((state) => state.contactOpen);

  const [sucess, sucessSetter] = useState(false);
  const [fields, fieldsSetter] = useState({
    email: null,
    emailError: null,
    message: null,
    messageError: null,
  });

  useEffect(() => {
    // open the nav on page load if url has contact hash
    if (window.location.hash === '#contact') {
      dispatch({
        type: 'TOGGLE_CONTACT',
        payload: true,
      });
    }

    document.addEventListener('keydown', handleEscape, false);
  }, []);

  const handleEscape = (e) => {
    if (e.keyCode === 27) dispatch({ type: 'TOGGLE_CONTACT', payload: false });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    fieldsSetter({
      ...fields,
      emailError: null,
      messageError: null,
    });

    if (!fields.email || !fields.message) return false;

    // validate email
    const isEmailValid = useEmailValidator(fields.email);
    if (isEmailValid.error !== false) {
      fieldsSetter({
        ...fields,
        emailError: isEmailValid.message,
        messageError: null,
      });

      return false;
    }

    // validate password
    const isMessageValid = !(
      fields.message &&
      fields.message.length > 3 &&
      fields.message.length < 300
    );
    if (isMessageValid !== false) {
      fieldsSetter({
        ...fields,
        emailError: null,
        messageError: 'Please enter a valid message',
      });

      return false;
    }

    // valid, send it out
    const res = await fetch('/api/subscribe', {
      body: JSON.stringify({
        email: fields.email,
        message: fields.message,
      }),
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'POST',
    });

    const { error } = await res.json();

    if (error) {
      fieldsSetter({
        ...fields,
        emailError: error.title,
        messageError: null,
      });

      return false;
    } else {
      sucessSetter(true);
    }
  };

  if (contactOpen !== true) return <div id="Contact-loading" />;

  const inputClasses = 'p-2 bg-transparent border-0 border-b-2 border-nwcGreen';

  return (
    <section id="Contact" className="relative z-50">
      <Mask
        callback={() => {
          dispatch({ type: 'TOGGLE_CONTACT', payload: false });
        }}
        color="rgb(0,0,0,0.8)"
        gradient="bg-gradient-to-r from-purple-900 to-blue-700"
        visible
      >
        {sucess === false ? (
          <form onSubmit={(e) => handleSubmit(e)}>
            <br />
            <br />
            <strong className="text-7xl">CONTACT US</strong>

            <br />
            <br />
            <br />
            <br />
            <Input
              error={fields.emailError}
              placeHolder="Your Email"
              size={50}
              type="email"
              callback={(v) => fieldsSetter({ ...fields, email: v })}
              id="email"
              populatedValue={fields.email}
              theme=""
              title={null}
              inputTheme={inputClasses}
            />

            <br />
            <br />
            <br />
            <br />

            <Input
              error={fields.messageError}
              placeHolder="Your Message"
              size={50}
              type="text"
              callback={(v) => fieldsSetter({ ...fields, message: v })}
              id="message"
              populatedValue={fields.message}
              theme=""
              title={null}
              inputTheme={inputClasses}
            />

            <br />
            <br />
            <br />
            <button
              type="submit"
              className="block text-center mx-auto bg-white text-black p-3 rounded-full"
            >
              SEND US A MESSAGE
            </button>
            <br />
            <button
              type="submit"
              className="block text-center text-sm mx-auto"
              onClick={() =>
                dispatch({ type: 'TOGGLE_CONTACT', payload: false })
              }
            >
              Cancel
            </button>
          </form>
        ) : (
          <div>
            <br />
            <br />
            <strong className="text-4xl">
              Thanks so much!
              <br />
              We&apos;ll get back to you shortly!
            </strong>
          </div>
        )}
      </Mask>
    </section>
  );
};

export default Contact;
