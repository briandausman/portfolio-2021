import { footerData as footer } from '../../data/footer-data';

const DnD: React.FC = () => {
  if (!footer) return <div id="DnD-loading" />;

  return (
    <section id="DnD">
      <strong className="text-gray-900">{footer.dnd}</strong>
      <div
        dangerouslySetInnerHTML={{
          __html: JSON.parse(JSON.stringify(footer.contactInfo)),
        }}
      />
    </section>
  );
};

export default DnD;
