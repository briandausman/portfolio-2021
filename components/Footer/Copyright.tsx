import { footerData as footer } from '../../data/footer-data';

const Copyright: React.FC = () => {
  if (!footer) return <div id="Copyright-loading" />;
  return (
    <section id="Copyright" className="text-gray-600 text-sm p-4 leading-loose">
      {new Date().getFullYear()} {footer.copyright}
      <br />
      {footer.tagline}
    </section>
  );
};

export default Copyright;
