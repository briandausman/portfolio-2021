import DnD from './DnD';
import SocialWrap from './SocialWrap';
import Copyright from './Copyright';

const FooterWrap: React.FC = () => (
  <footer
    id="FooterWrap"
    className="pt-28 pb-28 container mx-auto text-center leading-loose"
  >
    <DnD />

    <SocialWrap />

    <Copyright />
  </footer>
);

export default FooterWrap;
