import Image from 'next/image';
import Link from 'next/link';

import { footerData as footer } from '../../data/footer-data';

const SocialWrap: React.FC = () => {
  if (!footer) return <div id="SocialWrap-loading" />;

  return (
    <section id="SocialWrap">
      {footer.social.map((s) => (
        <Link href={s.url} key={s.title}>
          <a target="_blank" rel="noopener noreferrer" className="p-4">
            <Image height={25} width={25} src={s.icon} alt={s.title} />
          </a>
        </Link>
      ))}
    </section>
  );
};

export default SocialWrap;
