// add american freight
// add nwc 2019
// add pi click from old portfolio

const db = {
  portfolio: {
    projects: [
      {
        bg: '/images/hero--bg@2x.jpg',
        mainImage: '/images/portfolio/portfolio--upendo@2x.png',
        slug: 'upendo-staffing',
        thumbnail: '/images/portfolio/browser--upendo@2x.jpg',
        title: 'Upendo Staffing',
        deviceImage: '/images/hero--phone@2x.png',
        tech: ['Web Dev.', 'Jamstack.', 'Design & UX.'],
        launched: '2020',
        description:
          'Upendo Staffing is a recruiting firm based in Florida that previously had a simple, static Wix website. We gave them a jolt & hooked them up with a SUPER-sweet Jamstack site that allows them full control of their content as well as a job board to attract potential recruits and improve their workflow!',
        stack: '[Gatsby, React, GraphQl, Adobe Xd]',
      },
      {
        slug: 'daily-fit-blend',
        thumbnail: '/images/portfolio/thumb--daily-fit-blend.jpg',
        title: 'Daily Fit Blend',
      },
      {
        slug: 'nerd-fit',
        thumbnail: '/images/portfolio/thumb--nerd-fit.jpg',
        title: 'Nerd Fit',
      },
      {
        slug: 'sears-outlet',
        thumbnail: '/images/portfolio/thumb--sears-outlet.jpg',
        title: 'Sears Outlet',
      },
      {
        slug: 'chamberlain-staffing',
        thumbnail: '/images/portfolio/thumb--chamberlain.jpg',
        title: 'Chamberlain Staffing',
      },
      {
        slug: 'axiom-tech-group',
        thumbnail: '/images/portfolio/thumb--axiom.jpg',
        title: 'Axiom Tech Group',
      },
      {
        slug: 'camp-play-a-lot',
        thumbnail: '/images/portfolio/thumb--camp-play-alot.jpg',
        title: 'Camp Play A Lot',
      },
      {
        slug: 'tpsr-alliance',
        thumbnail: '/images/portfolio/thumb--tpsr.jpg',
        title: 'TPSR Alliance',
      },
      {
        slug: 'premier-academy',
        thumbnail: '/images/portfolio/thumb--premier-academy.jpg',
        title: 'Premier Academy',
      },
      {
        slug: 'ladse',
        thumbnail: '/images/portfolio/thumb--ladse.jpg',
        title: 'LADSE',
      },
      {
        slug: 'ob-chamber',
        thumbnail: '/images/portfolio/thumb--ob-chamber.jpg',
        title: 'Oak Brook Chamber of Commerce',
      },
      {
        slug: 'sos-illinois',
        thumbnail: '/images/portfolio/thumb--sos-illinois.jpg',
        title: 'SOS Illinois',
      },
      {
        slug: 'Stericycle',
        thumbnail: '/images/portfolio/thumb--stericycle.jpg',
        title: 'Stericycle',
      },
      {
        slug: 'bfg-tech',
        thumbnail: '/images/portfolio/thumb--bfg.jpg',
        title: 'BFG Tech',
      },
      {
        slug: 'niu-outreach',
        thumbnail: '/images/portfolio/thumb--niu-outreach.jpg',
        title: 'NIU Outreach',
      },
    ],
  },
};

export default db;
