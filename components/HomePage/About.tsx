import { useEffect, useState } from 'react';
import Image from 'next/image';
import Fade from 'react-reveal/Fade';

import { aboutData as about } from '../../data/about-data';

import { GradientText, useIsInView } from '../component-system';

const About: React.FunctionComponent = () => {
  const [fadeIn, fadeInSetter] = useState(false);
  const [rq, rqSetter] = useState(null);

  useEffect(() => {
    document.addEventListener('scroll', setterFunc);
    rqSetter(
      about.randomQuote[Math.floor(Math.random() * about.randomQuote.length)],
    );
  }, []);

  const setterFunc = () => {
    if (useIsInView('About') === true) fadeInSetter(true);
  };

  if (!about || !about.bg) return <div id="About-loading" />;

  return (
    <section
      id="About"
      className="container mx-auto pt-32"
      style={{
        backgroundImage: `url(${about.bg})`,
        backgroundPosition: 'center right',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'contain',
      }}
    >
      <div className="md:flex">
        <Fade left when={fadeIn === true}>
          <div className="md:flex-1 text-right pr-10">
            {about &&
              about.tagline.map((v) => (
                <h2
                  key={v}
                  className="text-3xl md:text-8xl font-bold leading-none"
                >
                  {v}
                </h2>
              ))}

            <div className="text-2xl">
              <GradientText text={rq} />
            </div>
          </div>
        </Fade>
        <Fade right when={fadeIn === true}>
          <div className="md:flex-1 text-center align-middle	inline-block">
            <br />
            <Image
              width={518}
              height={370}
              src={about.image}
              alt="About Us Image"
            />
          </div>
        </Fade>
      </div>
    </section>
  );
};

export default About;
