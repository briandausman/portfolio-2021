import Image from 'next/image';
import Fade from 'react-reveal/Fade';
import { useSelector } from 'react-redux';

import { GradientText } from '../component-system';

import { heroData as hero } from '../../data/hero-data';

const Hero: React.FC = () => {
  const mq = useSelector((state) => state.mq);

  if (!hero) return <div id="Hero-loading" />;
  return (
    <section
      id="Hero"
      className="relative text-center pt-32 leading-none text-6xl md:text-big font-bold h-screen z-20"
      style={{
        backgroundImage: `url(${hero.bg})`,
        backgroundPosition: '50% 50%',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        minHeight: 400,
      }}
    >
      <Fade top delay={500}>
        <div>
          <GradientText
            from={hero.tagline[0].from}
            to={hero.tagline[0].to}
            text={hero.tagline[0].title}
          />
          <br />
          <GradientText
            from={hero.tagline[1].from}
            to={hero.tagline[1].to}
            text={hero.tagline[1].title}
          />
          <br />
          <GradientText
            from={hero.tagline[2].from}
            to={hero.tagline[2].to}
            text={hero.tagline[2].title}
          />
        </div>
      </Fade>

      <div
        className={
          mq === 'mobile' ? 'absolute w-screen' : 'absolute -bottom-36 w-screen'
        }
      >
        <Fade bottom delay={500}>
          <Image
            className="pt-3"
            src={hero.image}
            alt={hero.alt}
            width={720}
            height={1222}
          />
        </Fade>
      </div>
    </section>
  );
};

export default Hero;
