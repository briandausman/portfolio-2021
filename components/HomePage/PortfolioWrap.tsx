import Image from 'next/image';
import { useEffect, useState } from 'react';

import { portfolioData as portfolio } from '../../data/portfolio-data';
import { works } from '../../data/works';

const PortfolioWrap: React.FC = () => {
  const [currentBg, currentBgSetter] = useState(null);

  const bgs = [
    'bg-nwcBlue',
    'bg-nwcPink',
    'bg-nwcPurple',
    'bg-nwcGreen',
    'bg-nwcYellow',
    'bg-nwcOrange',
  ];

  useEffect(() => {
    currentBgSetter(bgs[Math.floor(Math.random() * bgs.length)]);
  }, []);

  if (!portfolio || !works || works.length < 1) return <div id="portfolio" />;

  return (
    <section
      className="mx-auto pt-32 grid grid-cols-1 md:grid-cols-3 gap-2"
      id="portfolio"
    >
      {works &&
        works.map((p, i) => (
          <div key={i} className={currentBg}>
            {/* <Link href={`/work/${p.slug}`}>
              <a> */}
            <Image
              src={p.thumbnail}
              alt={`See our work for ${p.workObj.title}`}
              height={354}
              width={619}
              layout="responsive"
              className="transition-all hover:opacity-50"
            />
            <div className="sr-only">{`View  our work for ${p.workObj.title}`}</div>
            {/* </a>
            </Link> */}
          </div>
        ))}
    </section>
  );
};

export default PortfolioWrap;
