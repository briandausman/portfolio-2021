import Image from 'next/image';

import { portfolioData as portfolio } from '../../data/portfolio-data';

const OlderWorks: React.FC = () => {
  if (!portfolio || !portfolio.olderWorks) return <div id="olderWorks" />;

  return (
    <section
      className="mx-auto pt-32 grid grid-cols-2 md:grid-cols-3 gap-2 text-right"
      id="olderWorks"
    >
      <div className="text-2xl md:text-6xl font-bold leading-none mr-4">
        <br />
        {"Other People We've Worked With"}
      </div>

      {portfolio.olderWorks &&
        portfolio.olderWorks.map((p, i) => (
          <div key={i}>
            <Image
              src={p.src}
              alt={`See our work for ${p.title}`}
              height={354}
              width={619}
              layout="responsive"
              className="transition-all hover:opacity-50"
            />
          </div>
        ))}
    </section>
  );
};

export default OlderWorks;
