import { useEffect, useState } from 'react';
import Fade from 'react-reveal/Fade';

import { services } from '../../data/services-data';

import { GradientText, useIsInView } from '../component-system';

const Services: React.FC = () => {
  const [fadeIn, fadeInSetter] = useState(false);

  useEffect(() => {
    document.addEventListener('scroll', setterFunc);
  }, []);

  const setterFunc = () => {
    if (useIsInView('services-text') === true) fadeInSetter(true);
    if (useIsInView('services-text') === true) fadeInSetter(true);
  };

  if (!services || !services.bg) return <div id="Services-Loading" />;

  return (
    <section className="mx-auto pt-32" id="Services">
      <div
        style={{
          backgroundImage: `url(${services.bg})`,
          backgroundPosition: '50% 50%',
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'cover',
        }}
      >
        <div
          id="services-text"
          className="container mx-auto pt-20 pb-20 align-middle text-center"
        >
          <Fade when={fadeIn === true}>
            <div className="text-4xl md:text-7xl">
              <strong>
                <GradientText
                  from="#FF38DA"
                  to="#FFFF00"
                  text={services.tagline1}
                />
                <br />
                <GradientText
                  from="#FF38DA"
                  to="#FFFF00"
                  text={services.tagline2}
                />
              </strong>
            </div>
          </Fade>
          <br />
          <Fade when={fadeIn === true}>
            <div className="text-white text-sm md:text-lg md:leading-loose pt-8">
              {services.copy}
            </div>
          </Fade>
        </div>
      </div>
      <br />
      <br />
      <br />
      <Fade>
        <div
          style={{
            backgroundImage: `url(${services.listBg})`,
            backgroundPosition: 'center 24px',
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'contain',
            minHeight: 400,
          }}
        >
          <img
            src={services.logos}
            alt="Services logos"
            className="md:max-w-screen-sm mx-auto -mt-24"
          />
          <div className="grid md:grid-cols-2 container mx-auto text-center pt-24">
            <div>
              <h2 className="text-gray-700 text-xl md:text-4xl">
                <strong>Development</strong>
              </h2>
              <br />
              <ul>
                {services.development.map((d) => (
                  <li key={d} className="text-gray-600 leading-loose text-xl">
                    {d}
                  </li>
                ))}
              </ul>
              <br />
              <br />
            </div>
            <div>
              <h2 className="text-gray-700 text-xl md:text-4xl">
                <strong>Design & SEO</strong>
              </h2>
              <br />
              <ul>
                {services.design.map((m) => (
                  <li key={m} className="text-gray-600 leading-loose text-xl">
                    {m}
                  </li>
                ))}
              </ul>
              <br />
              <br />
            </div>
          </div>
        </div>
      </Fade>
    </section>
  );
};

export default Services;
