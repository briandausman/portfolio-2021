import Link from 'next/link';

const BackButton: React.FC = () => (
  <section id="BackButton" className="text-center">
    <br />
    <br />
    <Link href="/#PortfolioWrap">
      <a className="text-md border border-gray-200 pt-3 pb-3 pr-10 pl-10 rounded-full shadow-sm hover:shadow-md transition-all text-nwcPurple text-12">
        {'Back to Portfolio'}
      </a>
    </Link>
  </section>
);

export default BackButton;
