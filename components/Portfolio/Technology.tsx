import Image from 'next/image';

interface Props {
  d: any;
}

const Technology: any = ({ d }: Props) => (
  <section id="Technology" className="container mx-auto pt-32">
    <div className="md:flex">
      <div className="md:flex-1 text-right pr-10 align-middle ">
        <div className="text-2xl pt-8">
          {d.workObj.tech.map((t) => (
            <h2 key={t} className="text-3xl md:text-8xl font-bold leading-none">
              {t}
            </h2>
          ))}
          <div className="text-3xl text-gray-400">{`Launched ${d.workObj.launched}`}</div>
        </div>
      </div>

      <div className="flex-1 text-center align-middle inline-block">
        <br />
        <span>
          <Image
            width={313}
            height={485}
            src={d.workObj.deviceImage}
            alt={`${d.workObj.title} device shot`}
          />
        </span>
      </div>
    </div>
  </section>
);

export default Technology;
