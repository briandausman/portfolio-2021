import Image from 'next/image';
import Fade from 'react-reveal/Fade';
import { useSelector } from 'react-redux';

interface DataObject {
  workObj: WorkObject;
}
interface Props {
  d: DataObject;
}

interface WorkObject {
  bg: string;
  gradientFrom: string;
  gradientTo: string;
  title: string;
  mainImage: string;
}

const PortfolioHeading: React.FC<Props> = ({ d }) => {
  const mq = useSelector((state) => state.mq);

  if (!d) return <div id="Hero-loading" />;

  return (
    <section
      id="Hero"
      className="relative text-center pt-20 leading-none text-7xl font-bold h-screen z-20"
      style={{
        backgroundImage: `url(${d.workObj.bg})`,
        backgroundPosition: '50% 50%',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        minHeight: 400,
      }}
    >
      <Fade top delay={500}>
        <div>
          <br />
          <h2 className="text-white">{d.workObj.title}</h2>
        </div>
      </Fade>

      <div
        className={
          mq === 'mobile'
            ? 'absolute w-screen mt-10'
            : 'absolute -bottom-20 w-screen'
        }
      >
        <Fade bottom delay={500}>
          <div className="container mx-auto">
            <Image
              src={d.workObj.mainImage}
              alt={`${d.workObj.title} project overview`}
              width={1353}
              height={1022}
            />
          </div>
        </Fade>
      </div>
    </section>
  );
};

export default PortfolioHeading;
