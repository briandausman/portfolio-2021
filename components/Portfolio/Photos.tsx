import Image from 'next/image';
import { useSelector } from 'react-redux';

interface WorkObject {
  browserImage: string;
  title: string;
}
interface DataObject {
  mainImage: string;
  workObj: WorkObject;
}
interface Props {
  d: DataObject;
}

const Photos: React.FC<Props> = ({ d }) => {
  const mq = useSelector((state) => state.mq);

  if (!d) return <div id="Photos-loading" />;

  return (
    <section id="Photos" className="text-center -mt-40 container mx-auto">
      <div className="shadow-lg rounded-md overflow-hidden">
        <Image
          className="pt-3"
          src={d.workObj.browserImage}
          alt={d.workObj.title}
          width={mq === 'mobile' ? 640 : 1280}
          height={mq === 'mobile' ? 448 : 856}
        />
      </div>
    </section>
  );
};

export default Photos;
