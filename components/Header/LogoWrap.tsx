import Link from 'next/link';

const LogoWrap: React.FC = () => (
  <section id="LogoWrap" className="text-right pt-3" style={{ height: 22 }}>
    <Link href="/">
      <a className="transition-all hover:opacity-50">
        <img
          src="/images/logo.svg"
          alt="Nerds With Charisma"
          width={45}
          height={22}
          className="inline"
        />
      </a>
    </Link>
  </section>
);

export default LogoWrap;
