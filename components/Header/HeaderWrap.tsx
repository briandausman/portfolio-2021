import LogoWrap from './LogoWrap';
import MenuWrap from './MenuWrap';

const HeaderWrap: React.FC = () => (
  <header id="HeaderWrap" className="fixed w-full z-50">
    <section className="container pl-3 md:pl-0 pr-3 md:pr-0 mx-auto pt-12 grid grid-cols-2">
      <section>
        <MenuWrap />
      </section>
      <section>
        <LogoWrap />
      </section>
    </section>
  </header>
);

export default HeaderWrap;
