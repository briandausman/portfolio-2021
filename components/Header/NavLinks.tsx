import { useRouter } from 'next/router';
import { useDispatch } from 'react-redux';
import { navigation } from '../../data/navigation';

import { useScrollToId } from '../component-system';

const NavLinks: React.FC = () => {
  const router = useRouter();
  const dispatch = useDispatch();

  const navFunc = (item) => {
    if (item.slug === 'contact') {
      router.push('#contact', undefined, { shallow: true });
      dispatch({
        type: 'TOGGLE_CONTACT',
        payload: true,
      });
    } else {
      if (item.url.includes('#')) {
        useScrollToId(item.url.split('#')[1]);
      } else {
        router.push(item.url);
      }

      dispatch({
        type: 'TOGGLE_NAV',
        payload: false,
      });
    }
  };

  return (
    <section id="NavLinks" className="mt-20">
      {navigation.map((item) => (
        <div
          key={item.slug}
          className="text-white text-5xl mb-12 hover:text-nwcBlue"
        >
          <button
            className="w-screen focus:outline-white"
            type="button"
            onClick={() => navFunc(item)}
          >
            <strong>{item.title}</strong>
          </button>
        </div>
      ))}
    </section>
  );
};

export default NavLinks;
