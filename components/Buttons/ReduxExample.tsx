/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* Delete ME */
import React from 'react';
import useSWR from 'swr';

import { useDispatch } from 'react-redux';

const fetcher = async (path) => {
  const res = await fetch(path);

  return res.json();
};

const ReduxExample = () => {
  const dispatch = useDispatch();

  const { data, error } = useSWR('/api/example', fetcher);

  const testClick = () => {
    dispatch({
      type: 'UPDATE_TEST',
      payload: `Updated: ${new Date().toDateString()}`,
    });
  };

  return (
    <div className="max-w-7xl mx-auto py-12 px-4 sm:px-6 lg:py-16 lg:px-8 lg:flex lg:items-center lg:justify-between">
      <button onClick={() => testClick()}>Update store</button>

      <hr />
      <strong>Example Api Data: </strong>
      {data && !error && data.test && <span>{data.test}</span>}
    </div>
  );
};

export default ReduxExample;
