# Configure everything in config.js

• You can safely delete everything labelled "Delete Me", You should then either null or replace all "Replace Me" sections, most listed below

• Google Analytics - gaId - If null, tracking code will not be installed

• Google Tag Manager - gtmId - If null, gtm will not be loaded

• 404 page - Customize the layout in 404.tsx

• Robots.txt - Customize robots.txt if need be

• Sitemap.xml - Replace your domain with your website url in generate-sitemap.tsx and robots.txt

• Favicon - Replace in public directory

• Open Graph - og - Update in config.js, if null, will not print

• Schema Markup - schemaMarkup - lots of data in config.js, remove if you'd like. Constants that are reused are at the top

# Commands

Front-end local server, opens on port 3000
`npm run dev`

Front-end Build, production files
`npm run build`

Front-end storybook, see components in action
`npm run storybook`

Front-end start, start the application (usually in prod)
`npm run start`

Front-end lint, check code for errors
`npm run lint`

Front-end test, run jest test files
`npm run test`

Front-end e2e tests, start cypress and run end to end tests
`npm run cypress`

Back-end Strapi cms, opens on port 1337
`cd strapi`
`npm run develop`

Back-end start, start cms
`cd strapi`
`npm run start`

Back-end build, make stand alone cms files
`cd strapi`
`npm run build`
