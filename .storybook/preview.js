import React from 'react';
import 'tailwindcss/tailwind.css';

export const decorators = [
  (Story) => (
    <div>
      <Story />
    </div>
  ),
];
