// /* eslint-disable @typescript-eslint/explicit-module-boundary-types */
// import { useEffect } from 'react';
// import { useDispatch } from 'react-redux';

// import Fade from 'react-reveal/Fade';

// import { fetchData, Seo } from '../../components/component-system';

// import BackButton from '../../components/Common/BackButton';
// import Description from '../../components/Portfolio/Description';
// import Photos from '../../components/Portfolio/Photos';
// import PortfolioHeading from '../../components/Portfolio/PortfolioHeading';
// import Technology from '../../components/Portfolio/Technology';

// const Work: React.FC = ({ data }: any) => {
//   const dispatch = useDispatch();

//   useEffect(() => {
//     dispatch({
//       type: 'MQ',
//       payload: window.innerWidth < 1200 ? 'mobile' : 'desktop',
//     });
//   }, [data]);

//   return (
//     <section id="Work">
//       <Seo
//         domain={data.workObj.seo.domain}
//         gaId={data.workObj.seo.gaId}
//         googleMapsApiKey={data.workObj.seo.googleMapsApiKey}
//         googleTagManagerId={data.workObj.seo.googleTagManagerId}
//         meta={data.workObj.seo.meta}
//         og={data.workObj.seo.og}
//         protocol={data.workObj.seo.protocal}
//         title={data.workObj.seo.title}
//         heading={data.workObj.seo.title}
//       />

//       <PortfolioHeading d={data} />

//       <Fade bottom delay={500}>
//         <Technology d={data} />
//       </Fade>
//       <Fade bottom delay={500}>
//         <Description d={data} />
//       </Fade>
//       <Fade bottom delay={500}>
//         <Photos d={data} />
//       </Fade>
//       <BackButton />
//     </section>
//   );
// };

// export default Work;

// /* get all possible portfolio ids */
// const getAllIds = async () => {
//   const works = await fetchData('works');

//   if (!works) return false;

//   // console.log(works);
//   return works.map((project) => {
//     return {
//       params: {
//         id: project.slug,
//       },
//     };
//   });
// };

// /* build all the pages via the paths received above */
// export async function getStaticPaths() {
//   const paths = await getAllIds();
//   // console.log(paths);
//   return {
//     paths,
//     fallback: false,
//   };
// }

// /* get the specific port data */
// const getProjectData = async (id) => {
//   // // Combine the data with the id
//   const works = await fetchData('works');

//   const result = works.filter((obj) => {
//     return obj.slug === id;
//   });

//   console.log(result[0]);

//   return {
//     id,
//     ...result[0],
//   };
// };

// /* send the data to props */
// export async function getStaticProps({ params }) {
//   const data = await getProjectData(params.id);
//   return {
//     props: {
//       data,
//     },
//   };
// }

import React from 'react';

const Work: React.FC = () => <section>nulll</section>;

export default Work;
