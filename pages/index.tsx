import { Seo } from '../components/component-system';

import About from '../components/HomePage/About';
import Hero from '../components/HomePage/Hero';
import PortfolioWrap from '../components/HomePage/PortfolioWrap';
import Services from '../components/HomePage/Services';
import OlderWorks from '../components/HomePage/OlderWorks';

import { seo } from '../data/seo-data';

const HomePage: React.FC = () => {
  return (
    <section id="HomePage">
      <h1 className="sr-only">{seo.homepageTitle}</h1>
      <Seo
        domain={seo.domain}
        gaId={seo.gaId}
        googleMapsApiKey={seo.googleMapsApiKey}
        googleTagManagerId={seo.googleTagManagerId}
        meta={seo.meta}
        og={seo.og}
        protocol={seo.protocal}
        title={seo.homepageTitle}
        // heading={seo.homepageTitle}
      />

      <Hero />

      <About />

      <PortfolioWrap />

      <OlderWorks />

      <Services />
    </section>
  );
};

export default HomePage;
