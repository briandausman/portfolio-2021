import Document, { Html, Head, Main, NextScript } from 'next/document';

import config from '../components/lib/config';

export default class MyDocument extends Document {
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  render() {
    return (
      <Html lang="en">
        <Head>
          {config.schemaMarkup &&
            config.schemaMarkup.map((item, i) => (
              <script
                key={i}
                type="application/ld+json"
                dangerouslySetInnerHTML={{
                  __html: JSON.stringify(item).replace(/&quot;/g, '"'),
                }}
              />
            ))}
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
