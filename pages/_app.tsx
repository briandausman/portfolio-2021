import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { Provider } from 'react-redux';

import 'tailwindcss/tailwind.css';

import { useStore } from '../store';
import * as gtag from '../components/lib/googleAnalytics';

import Contact from '../components/Contact/Contact';
import FooterWrap from '../components/Footer/FooterWrap';
import HeaderWrap from '../components/Header/HeaderWrap';

interface Props {
  Component: React.ElementType;
  pageProps: any;
}

const App: React.FC<Props> = ({ Component, pageProps }) => {
  const store = useStore(pageProps.initialReduxState);
  const router = useRouter();

  useEffect(() => {
    const handleRouteChange = (url: URL) => gtag.pageview(url);

    router.events.on('routeChangeComplete', handleRouteChange);

    return () => {
      router.events.off('routeChangeComplete', handleRouteChange);
    };
  }, [router.events]);

  return (
    <Provider store={store}>
      <HeaderWrap />
      <Contact />

      <main id="main">
        <Component {...pageProps} />
      </main>

      <FooterWrap />
    </Provider>
  );
};

export default App;
