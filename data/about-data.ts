export const aboutData = {
  bg: '/images/about--bg@2x.jpg',
  image: '/images/about--logo@2x.png',
  randomQuote: [
    'Coding Since Before We Could Walk.',
    'The Best in the World at What We Do.',
    'Super-Awesome Apps for Super-Awesome Peeps.',
    "I Think We're Gonna Need A Bigger Boat.",
    'Coding Ninjas.',
    'Punk Rock Pixels.',
    'The Best There Is, Was, and Ever Will Be.',
    'Do Or Do Not. There Is No Try.',
    'We Know "The Google".',
    'FOR SCIENCE!',
    'Reinforced by Space-Age Technology.',
  ],
  tagline: ['websites.', 'apps.', 'seo.', 'better.'],
};
