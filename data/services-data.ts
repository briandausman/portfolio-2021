export const services = {
  bg: '/images/services--bg@2x.jpg',
  copy:
    "Nerds With Charisma are a small band of designers, developers, & creatives. Our unique process involves a hands on, back-and-forth approach to get your project off the ground and into the wild exactly as you envision it. We can bring fresh ideas and a new approach to your brand. We don't just build your website, we build and cultivate your online identity. We collaborate closely with you to learn your business, discover new opportunities, and bring your ideas to life. We’re also very comfortable...",
  listBg: '/images/services--color@2x.jpg',
  logos: '/images/services--logos.png',
  design: [
    'Business Cards',
    'Logos & Branding',
    'Stationary',
    'E-Blasts',
    'Social & Strategy',
    'Keyword Research',
    'Analytics',
    'And More..',
  ],
  development: [
    'NextJs / Gatsby',
    'JamStack',
    'Mobile Apps / React Native',
    'Wordpress Websites',
    'SEO & Marketing',
    'Analytics, Tagging, Tracking Pixels',
    'Backups & Monitoring',
    'eCommerce',
  ],
  tagline1: 'We Can Help',
  tagline2: 'Build Your Brand',
};
