export const portfolioData = {
  bg: '/images/hero--bg@2x.jpg',
  gradientFrom: '#FFEA4A',
  gradientTo: '#FFAD3C',
  olderWorks: [
    {
      src: '/images/portfolio/thumb--camp-play-alot.jpg',
      title: 'Camp Play-A-Lot',
    },
    {
      src: '/images/portfolio/thumb--premier-academy.jpg',
      title: 'Premier Acadamy',
    },
    {
      src: '/images/portfolio/thumb--ladse.jpg',
      title: 'LADSE',
    },
    {
      src: '/images/portfolio/thumb--ob-chamber.jpg',
      title: 'OakBrook Chamber',
    },
    {
      src: '/images/portfolio/thumb--sos-illinois.jpg',
      title: 'SOS Illinois',
    },
    {
      src: '/images/portfolio/thumb--stericycle.jpg',
      title: 'Stericycle',
    },
    {
      src: '/images/portfolio/thumb--bfg.jpg',
      title: 'BFG Tech',
    },
    {
      src: '/images/portfolio/thumb--niu-outreach.jpg',
      title: 'NIU Outreach',
    },
  ],
};
