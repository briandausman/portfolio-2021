export const navigation = [
  { isActive: true, slug: 'home', title: 'Home', url: '/' },
  { isActive: false, slug: 'portfolio', title: 'Portfolio', url: '#portfolio' },
  { isActive: false, slug: 'contact', title: 'Contact', url: '/contact' },
];
