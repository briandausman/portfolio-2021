export const footerData = {
  contactInfo:
    '<div><a href="/docs/brian-dausman-resume-2019.pdf"><h2 class="text-nwcPurple inline">brian dausman</h2></a> + <a href="http://www.andrewbieganski.com/images/andrewbieganski2018.pdf" target="_blank" rel="noopener noreferrer"><h2 class="text-nwcPurple inline">andrew bieganski</h2></a><br /><br /></div>',
  copyright: '© nerds with charisma',
  dnd: 'Designed & Developed By',

  tagline: "NWC making bitchin' websites since the 90s!",
  social: [
    {
      icon: '/images/logos/logo--gitlab.svg',
      title: 'GitLab',
      url: 'https://gitlab.com/nerds-with-charisma',
    },
    {
      icon: '/images/logos/logo--twitter.svg',
      title: 'Twitter',
      url: 'https://twitter.com/nerdswcharisma',
    },
    {
      icon: '/images/logos/logo--npm.svg',
      title: 'NPM',
      url: 'https://www.npmjs.com/~nerds-with-charisma',
    },
    {
      icon: '/images/logos/logo--medium.svg',
      title: 'Medium',
      url: 'https://medium.com/@nerdswithcharisma',
    },
    {
      icon: '/images/logos/logo--jedi.svg',
      title: 'Jamstack Jedi',
      url: 'https://jamstack-jedi.nerdswithcharisma.com/',
    },
  ],
};
