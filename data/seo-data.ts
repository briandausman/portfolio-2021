export const seo = {
  domain: 'nerdswithcharisma.com',
  gaId: 'G-12CYVEFHPV',
  googleMapsApiKey: 'AIzaSyCuke3pMotG7Ti70NQEwD6PiuCUzoOyIN4',
  googleTagManagerId: 'GTM-NP7BCNC',
  homepageTitle:
    'Nerds With Charisma. Awesome Websites, Brian Dausman & Andrew Bieganski',
  meta: [
    {
      content: '#663399',
      name: 'theme-color',
    },
    {
      content:
        'Nerds With Charisma is a cutting edge digital media agency specializing in awesome websites.',
      name: 'description',
    },
    {
      content:
        'Nerds with Charisma, Brian Dausman, Andrew Bieganski, Web Design Chicago, Awesome Websites, React Developers, Websites for Non-Profits, Bitchin Websites',
      name: 'keywords',
    },
    {
      content: 'summary_large_image',
      name: 'twitter:card',
    },
    {
      content: 'https://nerdswithcharisma.com',
      name: 'twitter:site',
    },
    {
      content: 'Brian Dausman',
      name: 'twitter:creator',
    },
    {
      content: 'Nerds With Charisma',
      name: 'twitter:title',
    },
    {
      content:
        'Nerds With Charisma is a cutting edge digital media agency specializing in awesome websites.',
      name: 'twitter:description',
    },
    {
      content: 'https://nerdswithcharisma.com/nwc-tile.jpg',
      name: 'twitter:image:src',
    },
    {
      content: '6b1bf384b5f509e1ffd1e8cf6307730c',
      name: 'p:domain_verify',
    },
  ],
  og: {
    description:
      'Nerds With Charisma is a cutting edge digital media agency specializing in awesome websites.',
    image: 'https://nerdswithcharisma.com/nwc-tile.jpg',
    site_name: 'Nerds With Charisma',
    title: 'Nerds With Charisma',
    type: 'website',
    url: 'https://nerdswithcharisma.com',
  },
  protocal: 'https:',
};
