export const heroData = {
  alt: 'NWC',
  bg: '/images/hero--bg@2x.jpg',
  image: '/images/hero--phone@2x.png',
  tagline: [
    {
      from: '#9300FF',
      title: 'NERDS',
      to: '#1FECFD',
    },
    {
      from: '#F3FE69',
      title: 'WITH',
      to: '#00EBFF',
    },
    { from: '#FFEA4A', title: 'CHARISMA', to: '#FFAD3C' },
  ],
};
